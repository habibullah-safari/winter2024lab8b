import java.util.Scanner;
public class BoardGameApp{
	public static void main(String [] args){
		Scanner reader = new Scanner(System.in);
	
		Board newBoard = new Board();
		
		System.out.println(" Welcome to Board Game Application !!!");
		
		int numCastles = 7;
		int turns = 0; 		
		
		System.out.println(newBoard);
		
		while(numCastles > 0 && turns < 8){
		// print the board	#castles and turns
		
		System.out.println( " number of castles left " +numCastles);
		System.out.println( " turns " + turns);
		
		// takes row and col input
		System.out.println(" Enter a row number ");
		int row = reader.nextInt();
		System.out.println(" Enter a column number ");
		int column = reader.nextInt();
		
		//place token in board
		int returnedValue = newBoard.placeToken(row, column);
		System.out.println(returnedValue);	
		
		// if the position is not empty
		while (returnedValue < 0){
			System.out.println(" Invalid row or column ");
			System.out.println(" Enter a row number ");
			row = reader.nextInt();
			System.out.println(" Enter a column number ");
			column = reader.nextInt();
	
			returnedValue = newBoard.placeToken(row, column);	
		}
		
		if (returnedValue == 1 ){
			System.out.println(" There was a wall in that position !!! ");
		}else if (returnedValue == 0){
			System.out.println(" A Castle was successfully placed !!! ");
			numCastles--;
		}
		
	
		//after placing token
		System.out.println(newBoard);
		
		// step f
		turns++;
		}
		
		System.out.println(newBoard);
		if(numCastles <= 0){
			System.out.println(" congratulation you won !!! ");
		}else {
			System.out.println(" Sorry you lost !!!");
		}
	}



}