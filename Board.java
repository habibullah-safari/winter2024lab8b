import java.util.Random;
public class Board {

    //fields 
    private Tile[][] grid;
    private final int size = 5;

    // constructor
    public Board() {
        this.grid = new Tile[this.size][this.size];
		
		Random random = new Random();
		
        for (int index = 0; index < this.size; index++) {
				int position =  random.nextInt(this.size);
				for (int j = 0; j < this.size; j++) {
					this.grid[index][j] = Tile.BLANK;
					
					this.grid[index][position] = Tile.HIDDEN_WALL;
					
            }
        }

    }

    //toStrig method
    public String toString() {
        String wholeBoard = "";

        for (int index = 0; index < this.grid.length; index++) {
            for (int j = 0; j < this.grid[index].length; j++) {
                wholeBoard += this.grid[index][j].getName() + " | ";
            }
            wholeBoard += "\n" + "________________________________________";
            wholeBoard += "\n" ;
            
        }

        return wholeBoard;
    }

    //placeToken
    public int placeToken(int row, int column) {
      int range =0;
	  
        if ((row < 0 || row >= this.size) && (column < 0 || column >= this.size)) {
            range = -2;
        }else if(this.grid[row][column] == Tile.CASTLE || this.grid[row][column] == Tile.WALL ){
			range = -1;
		}else if(this.grid[row][column] == Tile.HIDDEN_WALL ){
			this.grid[row][column] = Tile.WALL;
		}else if(this.grid[row][column] == Tile.BLANK){
			range = 0;
			this.grid[row][column] = Tile.CASTLE;
			System.out.println(" A CASTLE was place successfully");
		}
		return range;
		
	
    }
}
