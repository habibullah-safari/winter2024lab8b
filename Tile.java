
public enum Tile {

    BLANK("\u001B[36m" + "  _  " + "\u001B[0m"),
    WALL("\033[0;31m" + "  W  " + "\u001B[0m"),
    HIDDEN_WALL("\u001B[36m"  + "  _ " + "\u001B[0m"),
	CASTLE("\033[0;34m" + " C " + "\u001B[0m" )
	;

    //field
    private final String name;
    //constructor

    private Tile(String name) {
        this.name = name;
    }
    //getter

    public String getName() {
        return this.name;
    }
}
